# What I learnt

* [Getting set up](./docs/01-SETUP.md)
* [Fun with React](./docs/02-REACT.md)
* [Rethinking and redoing](./docs/03-RETHINKING.md)
* [Making a connection](./docs/04-SOCKETS.md)

## Resources

### Helpful docs

* https://reactpatterns.com/
* https://reactcheatsheet.com/
* https://serverless-stack.com/

### Official docs

* https://reactjs.org/ (especially the [tutorial](https://reactjs.org/tutorial/tutorial.html))
* https://eslint.org/
* https://undraw.co/
* http://expressjs.com/