import openSocket from 'socket.io-client';

const connection = openSocket('http://localhost:8080/');

function send(room, message, callback) {
  // Send a message to the room
  connection.on('connection', (socket) => {
    socket.in(room).emit('send', message, (data) => {
      // Data is confirmation response
      console.log(data);
      callback(data);
    });
  });
}

function receive(room, callback) {
  // listen for any messages coming through
  // of type 'receive' and then trigger the
  // callback function
  connection.on('connection', (socket) => {
    socket.in(room).on('receive', (message) => {
      // console.log the message for posterity
      console.log(message);
      // trigger the callback passed in when
      // our App component calls connect
      callback(message);
    });
  });
}

export default { connection, send, receive };
