import React, { Component } from 'react';
// import openSocket from 'socket.io-client';
import Game from '../game/game';
import Setup from '../setup/setup';
import Start from '../start/start';
import Connect from '../connect/connect';
import './app.css';

// const socket = openSocket('http://localhost:8080');

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: 'start', // statuses are: start, setup (new/join), choose, play, finish,
      cells: false,
      choice: false,
      isOwner: null,
      room: null
    };
  }

  onStart = (s) => {
    console.log(`Starting: ${s}`);
    // Go to server to start a game
    fetch('/api/start')
      .then(res => res.json())
      .then((start) => {
        this.setState({
          cells: start.cells,
          status: s,
          room: start.room
        });
        Connect.connection.on('connection', (skt) => {
          skt.join(start.room);
        });
      });
  }

  onSetup = (opts) => {
    console.log('Game set up', opts);
    const status = 'choose';
    this.setState({
      status,
      isOwner: opts.isOwner
    });
  }

  onChoose = (choice) => {
    console.log('Emoji chosen', choice);
    const status = 'play';
    this.setState({
      status,
      choice
    });
  }

  setStatus = (status) => {
    this.setState({
      status
    });
  }

  render() {
    const {
      cells,
      status,
      choice,
      isOwner,
      room
    } = this.state;
    const childProps = {
      cells,
      status,
      choice,
      isOwner,
      room,
      onStart: this.onStart,
      onSetup: this.onSetup,
      setStatus: this.setStatus
    };
    switch (status) {
      case 'choose':
      case 'play':
      // case 'play.me':
      // case 'play.you':
      // case 'play.response':
        return (
          <Game {...childProps} />
        );
      case 'setup':
      case 'new':
      case 'join':
        return (
          <Setup {...childProps} />
        );
      case 'start':
      default:
        return (
          <Start {...childProps} />
        );
    }
  }
}
