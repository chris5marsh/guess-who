import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cell from '../cell/cell';
import './grid.css';

export default class Grid extends Component {

  static defaultProps = {
    cellFunction: false,
    cells: false,
    status: null
  }

  static propTypes = {
    cellFunction: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    cells: PropTypes.arrayOf(PropTypes.object),
    status: PropTypes.string
  }

  constructor(props) {
    super(props);
    const {
      cells
    } = this.props;
    this.state = {
      cells
    };
  }

  handleClick = (i) => {
    console.log(`Clicked ${i}`);
    const {
      cellFunction,
      cells,
      status
    } = this.props;
    // Make copy of cells, resetting chosen param if still in choosing phase
    const cellsSlice = cells.map((cell) => {
      const c = cell;
      if (status === 'choose') {
        c.chosen = false;
      }
      return c;
    });
    const foundIndex = cellsSlice.findIndex(cell => cell.id === i);
    if (cellFunction === false) return false;
    if (foundIndex === -1) return false;
    if (status === 'choose') {
      cellsSlice[foundIndex].chosen = cellsSlice[foundIndex].chosen !== true;
    } else {
      cellsSlice[foundIndex].flipped = cellsSlice[foundIndex].flipped !== true;
    }
    this.setState({ cells: cellsSlice });
    return cellFunction(cellsSlice[foundIndex]); // Return the selected emoji to the parent function
  }

  renderCell = cell => (
    <Cell
      chosen={cell.chosen}
      flipped={cell.flipped}
      onClick={() => this.handleClick(cell.id)}
      id={cell.id}
      key={cell.id}
      name={cell.name}
      src={`public/svg/${cell.src}`}
    />
  )

  render() {
    const {
      cells
    } = this.state;
    const cellItems = cells.map(this.renderCell);
    return (
      <ul className="grid">
        {cellItems}
      </ul>
    );
  }

}
