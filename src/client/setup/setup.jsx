import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../header/header';
import Connect from '../connect/connect';
import './setup.css';

export default class Setup extends Component {

  static defaultProps = {
    status: null,
    onSetup: null
  };

  static propTypes = {
    status: PropTypes.string,
    onSetup: PropTypes.func,
    room: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    const {
      status
    } = this.props;
    let title;
    let message;
    let className;
    if (status === 'new') {
      title = 'Setting up a new game...';
      message = 'Your game has been set up! Your opponent should enter the code below';
      className = 'new';
      this.setupNew();
    } else if (status === 'join') {
      title = 'Joining an existing game...';
      message = 'Enter the code you\'ve been given in the box below.';
      className = 'join';
    }
    this.state = {
      title,
      message,
      className
    };
  }

  onChange = () => {
    const thisInput = document.getElementsByClassName('npt--join');
    const thisValue = thisInput[0].value;
    if (thisValue.length === 6) {
      // Go and join the game
      this.joinExisting(thisValue);
    }
  }

  createInput = (type) => {
    const {
      room
    } = this.props;
    if (type === 'new') {
      return (
        <input
          type="text"
          className={`npt npt--${type}`}
          value={room}
          disabled
        />
      );
    }
    return (
      <input
        type="text"
        className={`npt npt--${type}`}
        onChange={this.onChange}
        maxLength="6"
      />
    );
  }

  joinExisting = (room) => {
    const {
      onSetup
    } = this.props;
    const opts = {
      room,
      isOwner: false
    };
    // Go to socket, find 'room'
    console.log(`Joining room: ${room}`);
    Connect.send(room, 'Player joining!', (data) => {
      console.log(data);
      setTimeout(() => {
        onSetup(opts);
      }, 2000);
    });
  }

  setupNew = () => {
    const {
      onSetup,
      room
    } = this.props;
    const opts = {
      isOwner: true
    };
    console.log(`Got room: ${room}`);
    // Waiting to get a player
    /*
    setTimeout(() => {
      console.log('Player joined!');
      this.setState({
        message: 'A player has joined'
      });
      onSetup(opts);
    }, 4000);
    */
  }

  render() {

    const {
      title,
      message,
      className
    } = this.state;

    return (
      <div className="container">
        <Header />
        <div className={`setup setup--${className}`}>
          <h2 className="title">{title}</h2>
          <p>{message}</p>
          <p>{this.createInput(className)}</p>
        </div>
      </div>
    );

  }

}
