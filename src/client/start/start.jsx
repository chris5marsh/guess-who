import React from 'react';
import PropTypes from 'prop-types';
import Header from '../header/header';
import './start.css';

const Start = (props) => {

  const handleClick = (event) => {
    const { target: { value } } = event;
    const { onStart } = props;
    onStart(value);
  };

  return (
    <div className="container">
      <Header />
      <div className="start">
        <h2 className="title">Get started!</h2>
        <p>
          <em>Guess Who with Emoji</em>
          , also known as&nbsp;
          <em>GWE</em>
          &nbsp;or&nbsp;
          <em>Guess Whomoji</em>
          , is the old favourite game of Guess Who*,
          but played with emoji.
        </p>
        <p>
          You can start a new game, or join a game that
          someone else has set up. If you&rsquo;re joining
          a game you should have a six-digit code from the
          person who started the game. They get to go first.
        </p>
        <p>
          <small>
            * Not affiliated with Guess Who, Hasbro or
            anyone else. This is just for fun.
          </small>
        </p>
        <p>
          <button type="submit" className="btn btn--start" onClick={handleClick} value="new">Start a new game</button>
        </p>
        <p>
          <button type="submit" className="btn btn--join" onClick={handleClick} value="join">Join an existing game</button>
        </p>

      </div>
    </div>
  );
};

Start.defaultProps = {
  onStart: null
};

Start.propTypes = {
  onStart: PropTypes.func
};

export default Start;
