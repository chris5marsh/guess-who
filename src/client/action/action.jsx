import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Connect from '../connect/connect';
import './action.css';

export default class Action extends Component {

  static defaultProps = {
    choice: false,
    isOwner: false
  }

  static propTypes = {
    choice: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    isOwner: PropTypes.bool,
    status: PropTypes.string.isRequired,
    setStatus: PropTypes.func.isRequired,
    room: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props);
    const turn = props.isOwner === true ? 'me' : 'you';
    this.state = {
      turn,
      question: false,
      response: false
    };
    // Listen out for responses in this room
    Connect.receive(props.room, (data) => {
      // Receive answer
      console.log(data);
      // this.setState({
      //   turn: 'response',
      //   question,
      //   response: 'Yes' // or 'No' or 'Huh?'
      // });
    });
  }

  handleClick = (event) => {
    const action = document.getElementById('action').value;
    const { target: { value } } = event;
    const {
      choice,
      setStatus,
      room
    } = this.props;

    // Action can be 'choose', 'confirm', 'ask', or 'respond'

    // set status and send up the chain

    // Send question or response to socket
    if (action === 'choose') {
      console.log('Emoji chosen', choice);
      setStatus('play');
    } else if (action === 'ask') {
      const question = document.getElementById('question').value;
      this.setState({
        question
      });
      // Send question, get answer
      console.log(`Asking question: ${question}`);
      Connect.send(room, question, (data) => {
        console.log(data, '---again');
        // Prepare to receive answer
        this.setState({
          turn: 'response',
          question,
          response: 'Yes' // or 'No' or 'Huh?'
        });
      });
    } else if (action === 'respond') {
      this.setState({
        response: value
      });
      // Send response
      console.log(`Responding with: ${value}`);
      Connect.send(room, value, (data) => {
        console.log(data, '---again');
        if (value === 'yes' || value === 'no') {
          // Prepare to ask question if answered 'yes' or 'no'
          this.setState({
            turn: 'me'
          });
        } else if (value === 'huh') {
          // Prepare to answer again if answered 'huh'
          this.setState({
            turn: 'you'
          });
        }
      });
    } else if (action === 'confirm') {
      // Send confirmation, switch player
      // Wait to receive connection...
      this.setState({
        turn: 'you'
      });
    }
  }

  render() {
    const {
      turn,
      question,
      response
    } = this.state;
    const {
      status,
      choice
    } = this.props;
    let title = 'Waiting...';
    let message = false;
    let buttons = false;
    if (status === 'choose') {
      if (choice === false) {
        // Not yet chosen
        title = 'Please choose an emoji';
      } else {
        // Chose but not confirmed choice
        title = `Are you happy with ${choice.name} as your choice?`;
        buttons = (
          <div className="action__buttons">
            <input type="hidden" id="action" value="choose" />
            <button type="submit" className="btn" onClick={this.handleClick} id="choose-yes" value="yes">Yes please!</button>
            <button type="submit" className="btn" onClick={this.handleClick} id="choose-no" value="no">No way!</button>
          </div>
        );
      }
    } else if (status === 'play') {
      if (turn === 'you') {
        // Their turn
        title = `Your opponent asked: ${question}`;
        buttons = (
          <div className="action__buttons">
            <input type="hidden" id="action" value="respond" />
            <button type="submit" className="btn" onClick={this.handleClick} id="reply-yes" value="yes">Yes</button>
            <button type="submit" className="btn" onClick={this.handleClick} id="reply-no" value="no">No</button>
            <button type="submit" className="btn" onClick={this.handleClick} id="reply-huh" value="huh">?</button>
          </div>
        );
      } else if (turn === 'me') {
        // My turn
        title = 'Ask a question about your opponent\'s emoji';
        buttons = (
          <div className="action__buttons">
            <input type="hidden" id="action" value="ask" />
            <textarea id="question" className="action__question" />
            <button type="submit" className="btn" onClick={this.handleClick} id="question-ask" value="ask">Ask</button>
          </div>
        );
      } else if (turn === 'response') {
        // Getting a response
        title = 'You have a response!';
        message = (
          <div className="action__message">
            <p>
              You asked
              <strong>{question}</strong>
            </p>
            <p>
              Your opponent replied:
              <strong>{response}</strong>
            </p>
          </div>
        );
        buttons = (
          <div className="action__buttons">
            <input type="hidden" id="action" value="confirm" />
            <button type="submit" className="btn" onClick={this.handleClick} id="question-confirm" value="ok">OK</button>
          </div>
        );
      }
    } else {
      title = 'Waiting...';
    }
    return (
      <div className="action action--{status}">
        <h3 className="title">{title}</h3>
        {message !== false ? message : ''}
        {buttons !== false ? buttons : ''}
      </div>
    );
  }
}
