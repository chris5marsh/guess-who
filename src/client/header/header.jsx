import React from 'react';
import './header.css';

const Header = () => (
  <header id="header">
    <h1 id="logo" className="who">Guess Who</h1>
  </header>
);

export default Header;
