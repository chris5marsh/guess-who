import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './cell.css';

export default class Cell extends Component {

  static defaultProps = {
    chosen: false,
    flipped: false,
    onClick: null,
    name: null,
    src: 'public/sad.svg'
  }

  static propTypes = {
    chosen: PropTypes.bool,
    flipped: PropTypes.bool,
    onClick: PropTypes.func,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    src: PropTypes.string
  }

  state = {}

  handleClick = () => {
    const { onClick } = this.props;
    onClick();
  }

  render() {

    const {
      chosen,
      flipped,
      id,
      name,
      src
    } = this.props;

    let classname = '';
    if (chosen === true) classname += ' is-chosen';
    if (flipped === true) classname += ' is-flipped';

    return (
      <li className={`cell${classname}`}>
        <label className="cell__label" htmlFor={`cell-${id}`}>
          <img className="cell__image" src={src} alt={id} />
          <p className="cell__name who">{name}</p>
          <input className="cell__input" id={`cell-${id}`} type="checkbox" onClick={this.handleClick} />
        </label>
        <div
          className="cell__back"
          onClick={this.handleClick}
          onKeyPress={this.handleClick}
          role="button"
          tabIndex={0}
        >
          <p className="cell__backLogo who">
            Guess Who
          </p>
        </div>
      </li>
    );
  }
}
