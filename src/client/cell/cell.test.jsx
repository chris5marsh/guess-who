/* eslint-env jest */

// This is where the tests for the cell component go
import React from 'react';
import { shallow } from 'enzyme';
import Cell from './cell';

describe('cell_actions', () => {

  describe('makeCell', () => {
    test('Renders a cell component with some props', () => {
      const mockOnClick = jest.fn();
      const cell = shallow(
        <Cell
          clicked={false}
          onClick={mockOnClick}
          id={12345}
          name="Chris Marsh"
          imgSrc="default-user.svg"
        />
      );
      expect(cell).toMatchSnapshot();
    });
  });

  describe('clickCell', () => {
    test('successfully calls the onClick handler', () => {
      const mockOnClick = jest.fn();
      const cell = shallow(
        <Cell
          clicked={false}
          onClick={mockOnClick}
          id={12345}
          name="Chris Marsh"
          imgSrc="default-user.svg"
        />
      );

      cell.find('input').simulate('click');

      expect(cell.hasClass('is-selected')).toEqual(true);
    });
  });

  describe('chooseCell', () => {
    test('Clicking on a cell at the start of a game chooses it', () => {
      console.log('Choose cell!');
    });
  });
});
