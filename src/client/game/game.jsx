import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../header/header';
import Action from '../action/action';
import Grid from '../grid/grid';
import './game.css';

export default class Game extends Component {

  static defaultProps = {
    isOwner: false
  }

  static propTypes = {
    status: PropTypes.string.isRequired,
    setStatus: PropTypes.func.isRequired,
    room: PropTypes.string.isRequired,
    cells: PropTypes.arrayOf(PropTypes.object).isRequired,
    isOwner: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      title: 'Loading... Please wait!',
      choice: false
    };
  }

  makeChoice = (c) => {
    this.setState({
      choice: c
    });
  }

  switchOff = (c) => {
    console.log(c);
  }

  render() {

    const {
      title,
      choice
    } = this.state;
    const {
      status,
      cells,
      isOwner,
      setStatus,
      room
    } = this.props;

    const childProps = {
      cells,
      status,
      choice,
      isOwner,
      setStatus,
      room
    };

    if (status === 'choose') {
      childProps.cellFunction = this.makeChoice;
    } else {
      childProps.cellFunction = this.switchOff;
    }
    return (
      <div className="container">
        <Header />
        <Action {...childProps} />
        <Grid {...childProps} />
        <footer id="footer">
          <p>
            Built by&nbsp;
            <a href="https://twitter.com/chris5marsh">Chris Marsh</a>
          </p>
          <p>
            Emoji icons designed by&nbsp;
            <a href="https://www.freepik.com/">Freepik</a>
            &nbsp;from&nbsp;
            <a href="https://www.flaticon.com/">Flaticon</a>
          </p>
        </footer>
      </div>
    );
  }
}
