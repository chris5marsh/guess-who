const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const start = require('./start');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

app.use(express.static('dist'));

app.get('/api/getUsername', (req, res) => {
  res.send({ username: 'chris5marsh' });
});

app.get('/api/start', start);

io.on('connection', (socket) => {
  console.log('New client connected');
  socket.on('send', (from, msg) => {
    console.log('I received a message by ', from, ' saying ', msg);
  });
  socket.on('disconnect', () => console.log('Client disconnected'));
});

server.listen(8080, () => console.log('Listening on port 8080!'));
