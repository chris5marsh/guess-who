const shortid = require('shortid');
const Emoji = require('./emoji');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?');

const makeRoom = () => {
  // Make six digit room code
  const id = shortid.generate().substr(0, 6);
  // Check there isn't already a room called that
  return id;
};

const makeCells = () => {
  const r = [];
  const v = 24;
  const e = [...Emoji];
  let l = e.length;
  let i = 0;
  for (;i < v; i += 1) {
    const n = Math.floor(Math.random() * l);
    const thisEmoji = e.splice(n, 1)[0];
    thisEmoji.flipped = false;
    thisEmoji.chosen = false;
    l -= 1;
    r.push(thisEmoji);
  }
  return r;
};

const start = (req, res) => {
  const r = {};
  const cells = makeCells();
  const roomId = makeRoom();
  r.room = roomId;
  r.cells = cells;
  res.send(r);
};

module.exports = start;
