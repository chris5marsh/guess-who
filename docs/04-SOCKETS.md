## Sockets

* https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34
* https://www.valentinog.com/blog/socket-io-node-js-react/
* https://codeburst.io/isomorphic-web-app-react-js-express-socket-io-e2f03a469cd3

### Hooking up the connection

When should it connect? How do I get a unique room name?

* https://www.npmjs.com/package/shortid

### Using React contexts
* https://reactjs.org/docs/context.html
* https://itnext.io/how-to-use-a-single-instance-of-socket-io-in-your-react-app-6a4465dcb398
Probably overkill, prefer having a send and receive function