## Rethinking

### Quick break to learn about testing

* https://alligator.io/react/testing-react-redux-with-jest-enzyme/
* https://medium.com/codeclan/testing-react-with-jest-and-enzyme-20505fec4675
* https://scotch.io/tutorials/testing-react-components-with-enzyme-and-jest
* https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f

### Facebook login and friends list

Oh. Rethink needed. Aha! Emoji Guess Who! Using Flaticon emoji pack

* https://www.npmjs.com/package/react-facebook-login
* https://www.flaticon.com/packs/emoji-5

### Grinding away

Reading the React docs.

Figuring out state vs props, passing click handlers through to child components

### CSS fun

Wanted it to feel nice, so added flip transition with CSS. Couldn't do it without adding a `cell__back` element, which then needed the click handler, couldn't be empty, needed a role etc.

* https://davidwalsh.name/css-flip

### Moving some stuff to the server

Setting up new game best done on server and fed to both players
Temporary using an api call

* https://github.com/mattdesl/module-best-practices
* https://github.com/i0natan/nodebestpractices
* https://github.github.io/fetch/

### Action stations

Questions and answers

More diagrams

User A asks -> send question -> User B receives question, User B replies -> send response -> User A receives response, confirms, then vice versa

User flow - where do I hold state? How do I pass it doen to components?

Can it all be done within `action.jsx` instead of passing it up to game and then to app?

## TODO


### Keeping state on page refresh