## Getting set up

### Getting started

I started this project knowing almost nothing about React, except that it was a popular front-end UI renderer, used Javascript, and was invented by Facebook. After looking through my _list of cool ideas_ I decided that **Guess Who, but with Facebook** would be a good app to build using React - it was a pretty simple concept, had a few different composable bits of UI, wouldn't need a database, and could also use [socket.io](https://socket.io), which I'd seen before and thought looked interesting.

First off, I sketched out a few ideas, to get an idea of user flows and different UI elements:

[img]

### Planning ahead

Then, I wrote down the kind of technology I thought I might want to use for the front-end, back-end and development. I also wanted to make sure I set up some proper testing, and settled on using [Enzyme](https://airbnb.io/enzyme/) and [Jest](https://jestjs.io/)

[img]

Wow, that's a lot of buzzwords. I also had a think about how this would work once I was out of the development stage. I've used [Netlify](https://www.netlify.com/) for a couple of projects before, and liked the easy deployment method and simple setup. Plus, the cost os very attractive (it's free).

Armed with a little bit of forethought, a terminal window and an internet connection, I dived in.

### Diving in with Yarn and Webpack

I set up the project using `yarn init`, which gave me a good starting point. I knew that I would want to keep my client-side and server-side code separate, so I started with an `fe` and `be` folder, and started following the [tutorial on the React site](https://reactjs.org/tutorial/tutorial.html).

After a bit of tweaking and messing around, and figuring out what should be a `dependency` and what a `devDependency`, I had a setup that at least rendered some simple components. However, the next leap seemed a bit too far. I need some help.

* https://medium.com/appifycanada/migrate-to-webpack-from-grunt-bower-legacy-build-system-344526f47873
* https://www.pluralsight.com/guides/yarn-a-package-manager-for-node-js
* http://npm.github.io/using-pkgs-docs/package-json/types-of-dependencies.html
* https://medium.com/@raviroshan.talk/webpack-understanding-the-publicpath-mystery-aeb96d9effb1
* https://medium.com/@andrejsabrickis/modern-approach-of-javascript-bundling-with-webpack-3b7b3e5f4e7

### Using a Boilerplate

So I Googled "Boilerplate React Express Node" and, after a bit of trial and error, found Sandeep Raveesh's [Simple React Full Stack app](https://github.com/crsandeep/simple-react-full-stack). It took me a while to get my head round it, but I soon updated my folder structure to have `src/client` and `src/server` directories and started building the front end. It's confusing, as a front-end developer, not to just start adding in all the HTML that I know I'll need for a project, and build components. I've worked with Node before, and one of the biggest hurdles I've found is how to connect the differnt modules together. With React, though, there's a lot of help out there which explains how to pass `props` and `state` between a parent and child component.

Taking some bits wholesale from the boilerplate, and cherry-picking other bits that fitted my needs, I soon had an idea of how the project would develop.

* https://github.com/crsandeep/simple-react-full-stack

### Structuring the project

I wasn't sure how best to structure the project - should all the components have their own folder? where do stylesheets go? what about tests? - but, after doing some research, I decided on the structure you see in the `src` folder:

```
- src
  |- client
  |  |- index.jsx             (where it all starts)
  |  |- componentX            (each component in its own folder)
  |     |-componentX.jsx.     (component function)
  |     |-componentX.css.     (component styles)
  |     |-componentX.test.jsx (component tests)
  |- server
     |- index.js              (server setup)
     |- etc.                  (all the routes and import-able functions alongside it)
```

* https://medium.com/styled-components/component-folder-pattern-ee42df37ec68
* https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1
* https://daveceddia.com/react-project-structure/
* https://reactjs.org/docs/faq-structure.html

### Javascript templates

Not having used ES6 much in my day job, it was good to find my way around some of the new features, notably double arrow functions instead of anonymous `function`s, multi-line strings using backticks and being able to interpolate variables using the `${...}` syntax

### ESLint and error messages

I found the built-in settings for (ESLint)[https://eslint.org/] initially infuriating - always nagging at me that I'd done something wrong. Once I figured out _why_ I was bring nagged, though, it became a really useful tool, and makes me write more consistent code. I'm interested in bringing it into my work environment, to make sure everyone in my team writes code ~like me~ correctly.
