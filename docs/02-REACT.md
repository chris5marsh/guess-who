## React

### PropTypes

* https://wecodetheweb.com/2015/06/02/why-react-proptypes-are-important/
* https://medium.freecodecamp.org/react-pattern-centralized-proptypes-f981ff672f3b
* https://alligator.io/react/react-proptypes/

### Destructuring

### Illustrations and styles

* https://undraw.co/
* https://css-tricks.com/snippets/css/a-guide-to-flexbox/

### Presentational and container components

* https://engineering.musefind.com/our-best-practices-for-writing-react-components-dec3eb5c3fc8
* https://github.com/crsandeep/simple-react-full-stack
* https://itnext.io/react-component-class-vs-stateless-component-e3797c7d23ab
* https://css-tricks.com/learning-react-container-components/

### Routes and screens

N.B. adding `historyApiFallback: true` to webpack config, doing something similar in Express, so production server always serves index.html.

Stopped doing this because it wasn't working!

* https://reacttraining.com/react-router
* https://tylermcginnis.com/react-router-pass-props-to-components/

### CSS Variables

* https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables

### Fun with .jsx

Wanted it for syntax, had to change webpack config to include:

```javascript
/* eslint-disable */
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.js', '.json', '.jsx']
  }
```
