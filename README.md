# Guess Who, but with Emoji

## Getting started

Clone the repo, run `yarn` and wait for the all the node modules to download. You'll need to have yarn installed globally (`brew install yarn` if you're using Homebrew for Mac, which I am - use Chocolatey for Windows).

### Dev process

Running `yarn dev` will use `concurrently` to run the `client` and `server` processes separately (meaning the Webpack dev server can hot reload and Nodemon can restart the Express app whn it needs to). 

### Deploy process

Deploying to Netlify will run the `yarn start` script, which builds everything using Webpack, and the runs it all as a single server process.

### Testing

Things are still a little undeveloped, but the basic `test` script runs Jest, which will pick up any `*.test.jsx` files and test them. This process will be built into the build process when it's better developed.
